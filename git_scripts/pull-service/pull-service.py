#! /usr/bin/python

import time
import datetime
from subprocess import call

def main():
    while(True):
        call(['ssgit','pull'])
        
        sleeptime = (60 - datetime.datetime.now().minute) * 60
        print str(datetime.datetime.now()) + ' Sleeping ' + str(sleeptime) + 's'
        time.sleep(sleeptime)

if __name__ == "__main__":
    main()
