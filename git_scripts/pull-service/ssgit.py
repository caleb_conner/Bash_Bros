#! /usr/bin/python

import sys
import git
import os

initrepos = ["git@github.com:shipstation/shipstation.git"]

groups = []

class Repository:
    name = ""
    gitBase = None

    def __init__(self, name, repositoryPath):
        self.name = name
        self.gitBase = git.Repo(repositoryPath)
        
    def getCurrentBranch(self):
        return self.gitBase.active_branch

    # Returns an iterable list of branches
    def getBranches(self):
        return self.gitBase.branches
    
    def getName(self):
        return self.name

    def pull(self):
        ret = ""
        try:
            ret = self.gitBase.git.pull() 
        except Exception as ex:
            ret = str(ex.stderr)
        return ret
    
    def printBranches(self):
        print self.getName()
        localBranch = self.getCurrentBranch()
        for branch in self.getBranches():
            extra = "  "
            if(branch == localBranch):
                extra = " *"
            print extra + str(branch)

        print

    def printCurrentBranch(self):
        print self.getName()
        print "  " + str(self.getCurrentBranch())
        print

    def printAndPull(self):
        print "Pulling " + self.getName() + ":"
        print self.pull()
        print



class RepositoryGroup:
    name = ""
    repositories = []

    def __init__(self, name, repositories):
        self.name = name
        self.repositories = repositories

    def getName(self):
        return self.name

    def addRepository(self, repository):
        self.repositories.append(repository)

    def removeRepository(self, repository):
        self.repository.remove(repository)

    def getRepository(self, repository):
        for repo in self.repositories:
            if(repo.getName() == repository):
                return repo
        return None

    def getBranches(self):
        branches = []
        for repo in self.repositories:
            branches.append([repo.getName(),repo.getBranches(), repo.getCurrentBranch()])
        return branches

    def getCurrentBranch(self):
        branch = []
        for repo in self.repositories:
            branch.append([repo.getName(), repo.getCurrentBranch()])
        return branch

    def pull(self):
        for repo in self.repositories:
            repo.pull()

    def printBranches(self):
        for repo in self.repositories:
            repo.printBranches()

    def printCurrentBranch(self):
        for repo in self.repositories:
            repo.printCurrentBranch()

    def printAndPull(self):
        for repo in self.repositories:
            repo.printAndPull()

def getGroup(group):
    if(group == None):
        return None
    for g in groups:
        if(g.getName() == group):
            return g

    return None

def getRepositories():
    for repo in repositories:
        print repo.getName()

def getBranches(group):
    if(group == None):
        group = getGroup("all")
    group.printBranches()

def getBranch(group):
    if(group == None):
        group = getGroup("all")
    group.printCurrentBranch()

def pull(group):
    if(group == None):
        group = getGroup("all")
    group.printAndPull()
                
def getRepositoryOrGroup(group):
    for g in groups:
        if(g.getName() == group):
            return g
    g = getGroup("all")
    repository = g.getRepository(group)
    if(repository != None):
        return repository

    print group + " is not a repository or group of repositories."
    exit(1)
    

def initRepositoriesAndGroups():
    repoFile = open('/mnt/c/Users/caleb/GitHub/SSGit/repositories','r')
    repositories = []
    currentGroup = None
    for line in repoFile:
        r = line.split(" ");
        if(len(r) == 2):
            if(currentGroup != None):
                groups.append(currentGroup)
                currentGroup = None
            repositories.append(Repository(r[0].strip(),r[1].strip()))
        elif(len(line) > len(line.lstrip()) and len(line.strip()) > 0):
            if(currentGroup != None):
                for repo in repositories:
                    if(repo.getName() == line.strip()):
                        currentGroup.addRepository(repo)
                        break
        elif(len(r[0].strip()) > 0):
            currentGroup = RepositoryGroup(r[0].strip(),[])
            
    if(currentGroup != None):
        groups.append(currentGroup)

    groups.append(RepositoryGroup("all",repositories));

def createRepositories():
    if(len(sys.argv) != 3):
        print "Use: ssgit init root-repo-dir"
        print "Example: ssgit init C:\\repositories"

    dir = sys.argv[2]
    
    if(not os.path.exists(dir)):
        reply = raw_input(dir + " does not exist. Do you want to create it? y/n ")
        if(reply.lower() == 'y'):
            print "Creating Directory: " + dir
            os.mkdir(dir)
        else:
            print "Not creating directory"
            exit(0)
    
    os.chdir(dir)
    
    for r in initrepos:
        print git.Git().clone(r)
    
        

def main():
    if(len(sys.argv) > 1  and sys.argv[1] == "init"):
        createRepositories()

    initRepositoriesAndGroups()
    if(len(sys.argv) == 1):
        print "Use: SSgit [repositories|<current->branch <repository>|pull]"
    elif(sys.argv[1] == "repositories"):
        getRepositories()
    elif(sys.argv[1] == "branch"):
        repo = getRepositoryOrGroup("all")
        if(len(sys.argv) == 3):
            repo = getRepositoryOrGroup(sys.argv[2])
        getBranches(repo)
    elif(sys.argv[1] == "current-branch"):
        repo = None
        if(len(sys.argv) == 3):
            repo = getRepositoryOrGroup(sys.argv[2])
        getBranch(repo)
    elif(sys.argv[1] == "pull"):
        repo = None
        if(len(sys.argv) == 3):
            repo = getRepositoryOrGroup(sys.argv[2])
        pull(repo) 

if __name__ == "__main__":
    main()
