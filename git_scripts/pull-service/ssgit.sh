#! /bin/sh

case $1 in
    pull)
        for f in ./repositories/*
        do
            $f pull
            echo
        done
        ;;

    branch)

        for f in ./repositories/*
        do
            $f branch
            echo
        done

    

    *)
        echo $"Usage: $0 {pull|branch [new-branch]}"
	exit 1
esac
